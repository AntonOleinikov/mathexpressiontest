describe('Home', () => {

  it('Placing elements on the page', () => {
    cy.visit('/');

    cy.get('[data-cy=aside-container]')
      .should('contain', 'Previous results');

    cy.get('[data-cy=aside-container]')
      .should('contain', 'No expression results found');
  });

  it('Calc new expression: error', () => {
    cy.get('[data-cy=calc-expression-field]')
      .type('Failed expression');

    cy.get('[data-cy=calc-expression-submit]')
      .click();

    cy.get('[data-cy=aside-container]')
      .should('contain', 'No expression results found');

    cy.get('[data-cy=expression-result]')
      .should('not.exist');

    cy.get('[data-cy=card]')
      .should('have.length', 0);
  });

  it('Calc new expression: success', () => {
    cy.get('[data-cy=calc-expression-field]')
      .clear()
      .type('What is 5 plus 5?');

    cy.get('[data-cy=calc-expression-submit]')
      .click();

    cy.get('[data-cy=aside-container]')
      .should('not.contain', 'No expression results found');

    cy.get('[data-cy=expression-result]')
      .should('contain', '5 plus 5 equals 10');

    cy.get('[data-cy=card]')
      .should('have.length', 1);

    cy.get('[data-cy=card]')
      .first()
      .should('contain', 'What is 5 plus 5?')
      .should('contain', '10');
  });
});
