import React from 'react';
import { InputElement, InputContainer, ErrorMessage } from './TextField.styles';
import { TTextField } from './TextField.types';

const TextField = ({ error, ...rest }: TTextField) => {
  return (
    <InputContainer>
      <InputElement
        {...{
          error,
          ...rest
        }}
      >
      </InputElement>

      {
        error &&
        <ErrorMessage>{ error }</ErrorMessage>
      }
    </InputContainer>
  )
};

export default TextField;
