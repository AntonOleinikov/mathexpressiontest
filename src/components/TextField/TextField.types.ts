import { InputHTMLAttributes } from 'react';

export type TTextField = InputHTMLAttributes<HTMLInputElement> & {
  error?: string;
}
