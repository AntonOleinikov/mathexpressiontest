import styled from 'styled-components';

export const InputContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

export const ErrorMessage = styled.p`
  margin: 10px 0 10px 0;
  font-weight: 500;
  color: rgb(229, 115, 115);
`;

export const InputElement = styled.input<{error?: string}>`
  padding: 15px;
  background-color: rgb(253,254,254);
  border: none;
  border-radius: 7px;
  font-size: 16px;
  width: 100%;
  outline: none;
  
  &:focus {
    box-shadow: 0 0 0 2px rgb(106, 117, 202); 
  }
  
  ${({ error }) => !!error && `
    box-shadow: 0 0 0 2px rgb(229, 115, 115); 
    &:focus {
      box-shadow: 0 0 0 2px rgb(229, 115, 115); 
    }
  `}
`;

