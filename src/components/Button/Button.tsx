import React from 'react';
import { ButtonElement } from './Button.styles';
import { TButton} from './Button.types';

const Button = ({ children, ...rest }: TButton) => {
  return (
    <ButtonElement
      {...{
        ...rest
      }}
    >
      { children }
    </ButtonElement>
  )
};

export default Button;
