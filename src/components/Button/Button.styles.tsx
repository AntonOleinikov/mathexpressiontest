import styled from 'styled-components';

export const ButtonElement = styled.button`
  background-color: rgb(106,117,202);
  border-radius: 7px;
  font-size: 16px;
  border: none;
  padding: 15px 30px;
  color: rgb(255, 255, 255);
  cursor: pointer;
  
  &:hover {
    opacity: .9;
  }
`;

