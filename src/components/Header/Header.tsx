import React from 'react';
import { Container, InnerContainer, Logo } from './Header.styles';

const Header = () => {
  return (
    <Container>
      <InnerContainer className="container">
        <Logo href="/">
          Math Expression
        </Logo>
      </InnerContainer>
    </Container>
  )
};

export default Header;
