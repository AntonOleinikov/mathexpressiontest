export type TExpressionCard = {
  uuid: string;
  expression: string;
  result: number;
}
