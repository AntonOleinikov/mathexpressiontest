import React from 'react';
import { Container, QuestionText, AnswerText } from './Card.styles';
import { TExpressionCard } from './Card.types';

const Card = ({ expression, result }: TExpressionCard) => {
  return (
    <Container
      data-cy="card"
    >
      <QuestionText>
        { expression }
      </QuestionText>
      <AnswerText>
        { result }
      </AnswerText>
    </Container>
  )
};

export default Card;
