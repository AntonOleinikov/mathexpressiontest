import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: flex-start;
  justify-content: space-between;
  padding: 15px;
  border-radius: 7px;
  margin-bottom: 10px;
  background-color: rgb(215,220,237);
`;

export const QuestionText = styled.span`
  margin-bottom: 10px;
  width: 60%;
`;

export const AnswerText = styled.span`
  font-size: 28px;
  text-align: right;
  width: 40%;
`;

