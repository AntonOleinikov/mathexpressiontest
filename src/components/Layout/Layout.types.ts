export type TLayout = {
  aside: JSX.Element;
  header?: JSX.Element;
  content: JSX.Element;
}
