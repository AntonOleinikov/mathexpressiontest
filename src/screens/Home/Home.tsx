import React, {useState} from 'react';
import { Layout, ExpressionCard } from '@components';
import { ExpressionForm } from '@forms';
import { ExpressionResultsContainer, AsideContainer, ContentContainer, H3Title, ResultValue } from './Home.styles';
import { TExpressionCard } from "@components/Card/Card.types";
import { useLocalStorage } from '@hooks';
import { calcExpression } from '@utils';

const Home = () => {
  const [savedExpressions, setSavedExpressions] = useLocalStorage('expressionResults', []);
  const [errorMessage, setErrorMessage] = useState<string>('');
  const [expressionValue, setExpressionValue] = useState<string>('');
  const [lastExpressionResult, setLastExpressionResult] = useState<string>('');

  const onSubmit = (data: TExpressionCard) => {
    setSavedExpressions([
      data,
      ...savedExpressions
    ]);

    setExpressionValue('');
    setErrorMessage('');
  };

  const onChange = (expression: string) => {
    if (errorMessage) {
      setErrorMessage('');
    }

    if (lastExpressionResult) {
      setLastExpressionResult('');
    }
    setExpressionValue(expression);
  };

  const onValidate = (expressionText: string) => {

    try {
      const {
        uuid,
        result,
        preview,
        expression
      } = calcExpression(expressionText);

      onSubmit({
        uuid,
        result,
        expression
      });

      setLastExpressionResult(preview);
    } catch (e) {
      setErrorMessage('Expression has invalid format')
    }

  };

  return (
    <Layout
      aside={
        <AsideContainer
          data-cy="aside-container"
          className="container"
        >
          <H3Title>Previous results</H3Title>
          {
            savedExpressions && savedExpressions.length ?
              <ExpressionResultsContainer>
                {
                  savedExpressions.map(({ uuid, expression, result }: TExpressionCard) => (
                    <ExpressionCard
                      {...{
                        uuid,
                        expression,
                        result
                      }}
                      key={uuid}
                    />
                  ))
                }
              </ExpressionResultsContainer>
              :
              <h4>No expression results found</h4>
          }
        </AsideContainer>
      }
      content={
        <ContentContainer
          className="container"
        >
          <ExpressionForm
            hint={'Example: What is 1 plus 2?'}
            value={expressionValue}
            error={errorMessage}
            onChange={onChange}
            onSubmit={onValidate}
          />

          {
            lastExpressionResult &&
            <ResultValue
              data-cy="expression-result"
            >
              { lastExpressionResult }
            </ResultValue>
          }

        </ContentContainer>
      }
    />
  )
};

export default Home;
