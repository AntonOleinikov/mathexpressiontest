import { useState } from "react";
import { TExpressionCard } from '@components/Card/Card.types';

const useLocalStorage = (key: string, initialValue: any) => {
  const [storedValue, setStoredValue] = useState<any>(() => {
    try {
      const item = window.localStorage.getItem(key);
      return item ? JSON.parse(item) : initialValue;
    } catch (error) {
      return initialValue;
    }
  });

  const setValue = (value: TExpressionCard[]) => {
    try {
      setStoredValue(value);
      window.localStorage.setItem(key, JSON.stringify(value));
    } catch (error) {
      console.log('[useLocalStorage] error:', error);
    }
  };

  return [
    storedValue,
    setValue
  ];
};

export default useLocalStorage;
