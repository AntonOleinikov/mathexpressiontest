import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  align-items: flex-start;
  width: 100%;
`;

export const Hint = styled.p`
  font-size: 16px;
  margin-bottom: 10px;
  color: #757575;
`;
