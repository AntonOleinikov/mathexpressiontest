import React, { ChangeEvent, Fragment } from 'react';
import { Button, TextField } from '@components';
import { Container, Hint } from './ExpressionForm.styles';
import { TExpressionForm } from './ExpressionForm.types';

const ExpressionForm = ({value, hint, onSubmit, onChange, error}: TExpressionForm) => {

  const onKeyDown = (event: React.KeyboardEvent<HTMLDivElement>): void => {
    if (event.key === 'Enter') {
      event.preventDefault();
      event.stopPropagation();
      onSubmit(value);
    }
  };

  return (
    <Fragment>
      {
        hint &&
        <Hint>
          {hint}
        </Hint>
      }
      <Container>
        <TextField
          data-cy="calc-expression-field"
          autoFocus
          value={value}
          error={error}
          placeholder="Write the expression..."
          onChange={({target}: ChangeEvent<HTMLInputElement>) => {
            onChange(target.value);
          }}
          onKeyDown={onKeyDown}
        />

        <Button
          data-cy="calc-expression-submit"
          style={{
            marginLeft: 10
          }}
          onClick={() => {
            onSubmit(value)
          }}
        >
          Calc
        </Button>
      </Container>
    </Fragment>
  )
};

export default ExpressionForm;
